__autho__ = 'Teerasit Kasetkasem'
import numpy as np
def CFAR(line, r) :
    kernel = np.zeros((3*r),'float32')
    kernel[0:r] = 1./(2.*r)
    kernel[2*r:3*r] = 1./(2.*r)


    output = []
    ll =np.array(line)
    ll = ll[::-1]
    patch_size = r+ r/2

    end_point = len(line)+patch_size
    line2 = np.zeros(len(line)+3*r-1,'float32')
    line2[:patch_size] = ll[len(line)-patch_size-1:-1]
    line2[end_point:end_point+patch_size] = ll[1:patch_size+1]
    line2[patch_size:end_point] = line
    
    out = np.convolve(line2, kernel)
    output = out[3*r-1:3*r+len(line)-1]
    #print list(out2)
    #print list(line)
    
    #out = out[0:len(line)]
    return output



if __name__ == "__main__" :
    #f = open("/home/professor/Dropbox/UAV/DataFrame2.txt","r")
    f = open(r"DataFrame3.txt","r")
    z = []
    k = []
    for i in f:
        j = i.split('\t')
        bearing = j[12]
        k.append(map(int,j[14:]))
	
        if len(k)==14:
            z.append(k)
            k = [map(int,j[14:])]

    #print z[0]
    for i in range(len(z)):
        if i%2 == 0:
            z[i].reverse()
    z2 = np.array(z)
    print "max = ", z2.max()
    print "mean = ", z2.mean()

    th = 70
    print z2[0].shape
    #fig, ax = plt.subplots()
    #line, = ax.plot([], [])
    #line.set_xdata(np.arange(136))
    #plt.ylim(0 , 200)
    #plt.xlim(0 , 136)
    fig, ax2= plt.subplots(ncols=1, subplot_kw=dict(projection='polar'))
    first_run = True
    rept = 1
    #aa = np.array(z[0])
    #print cv.CV_FOURCC('M','P','E','G')
    #writer = cv2.VideoWriter() #(r"C:\Users\User\Dropbox\UAV\sonar.avi", cv.CV_FOURCC('M','P','E','G'), 25 ,(3*aa.shape[1],6*aa.shape[0]), True)
    #writer.open(r"C:\Users\User\Dropbox\UAV\sonar.avi", -1, 1 ,(3*aa.shape[1],6*aa.shape[0]), True)

    #print writer, writer.isOpened()
    raw_dat = z[0][0]
    th_a = CFAR(raw_dat, 21)
    CFAR_nbin = np.array(th_a).astype("float32")
    RAW_nbin = np.array(raw_dat).astype("uint8")
    
    im = np.zeros((a.shape[0],a.shape[1],3),'uint8')
    print CFAR_nbin
    
    
    """
    for a in z:
        th_a = CFAR(a, 31)
        th_a = np.array(th_a).astype("float32")
        a = np.array(a).astype("uint8")

        a = np.repeat(a, rept, axis =0)
        th_a = np.repeat(th_a, rept, axis =0)
        im = np.zeros((2*a.shape[0],a.shape[1],3),'uint8')
        im = im+255
        im[:a.shape[0],:,0] = a
        im[:a.shape[0],:,1] = a
        im[:a.shape[0],:,2] = a
        im[a.shape[0]:,:,0] = ((a < th_a*0) | (a<th))*255
        im[a.shape[0]:,:,1] = ((a < th_a*0) | (a<th))*255
        im = cv2.resize(im,(3*a.shape[1],6*a.shape[0]), interpolation = cv2.INTER_NEAREST)
        cv2.imshow("classified map",im)
        #writer.write(im)
        theta, r = np.mgrid[3*np.pi/8:5*np.pi/8:np.pi/(4*3*a.shape[0]), 0:5:5.0/(3*a.shape[1])]
        im2 = im[3*a.shape[0]:,:,0]

        cv2.imshow("raw data",a)
        cv2.waitKey(100)
	
        if first_run:
            first_run = True
            line = ax2.pcolormesh(theta, r, im2 )
        #else:
         #   line.set_array(im2)
        #line.set_ydata(a[7*rept,:])
        plt.show(block = False)
        plt.pause(0.001)
    #del writer
    """


