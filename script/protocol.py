import rospy
from zeabus_navigation.msg import Sonar_Protocol
     
if __name__ == "__main__":
        rospy.init_node("fake_sonar")
        pub = rospy.Publisher("/fake_ui",Sonar_Protocol,queue_size = 10)        
        msg = Sonar_Protocol()
        msg.Active = 1
        msg.Speed = 3
        msg.Gain = 50
        msg.nBin = 148
        msg.Adlow = 9 
        msg.Adspan = 12
        msg.Range = 5
        msg.Scantype = 0
        msg.Llim = 3200
        msg.Rlim = 6400
                     
        rospy.sleep(1)
        pub.publish(msg)
        rospy.spin()

