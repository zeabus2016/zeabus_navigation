#!/usr/bin/env python
'''
Created on Jan 22, 2015

@sonar driver 	by 	: Anol
@CFAR implement by 	: Teerasit Kasetkasem 
@integrated 	by 	: Siwakorn Tulsook
'''
import rospy
import serial , time
import numpy as np
import math
import time
from binascii import unhexlify
from std_msgs.msg import Int64MultiArray
from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray
from zeabus_navigation.msg import Sonar_Protocol
from sensor_msgs.msg import LaserScan

current_milli_time = lambda: int(round(time.time()))

class sonarDriver:
	def __init__(self,ser,publisher):
		rospy.Subscriber("/fake_ui",Sonar_Protocol,self.Callback)

		self.publisher = publisher
		self.CFARthres = 0.3
		self.RangeThres = rospy.get_param("sonarDriver/rangethres")
		self.IntenThres = rospy.get_param("sonarDriver/intenthres")
                self.BinStart = rospy.get_param("sonarDriver/binstart")
                self.Show_Log = rospy.get_param("sonarDriver/rawdata")

		self.windowsize = None
		self.Active = None
		self.Speed =None
		self.nBin = None
		self.Gain = None
		self.Adlow = None
		self.Adspan = None
		self.Range = None
		self.Scantype = None
		self.Adinterval = None
		self.dataSet = None
		self.ser = ser

		self.checkScantype = None
		self.checkRange = None
		self.checknBin = None

		self.laser_increment = None
		self.time_increment = None
		self.ranges = None
		self.laser_angle_min = None
		self.laser_angle_max = None

		self.HEADER = '\x40'
		self.TERM = '\x0A'

		#Byte index
	
		self.MSG_index = 10

		# Mostly using command
		self.mtVersionData = '\x01'  # 1
		self.mtHeadData = '\x02'  # 2
		self.mtAlive = '\x04'  # 4
		self.mtBBUserData = '\x06'  # 6
		self.mtReboot = '\x10'  # 16
		self.mtHeadCmd = '\x13'  # 19
		self.mtSendVersion = '\x17'  # 23
		self.mtSendBBUser = '\x18'  # 24
		self.mtSendData = '\x19'  # 25
		self.mtStopAlives = '\x42'  # 66

	def long_to_bytes (self,val, endianness='big'):
		"""
		Use :ref:`string formatting` and :func:`~binascii.unhexlify` to
		convert ``val``, a :func:`long`, to a byte :func:`str`.

		:param long val: The value to pack

		:param str endianness: The endianness of the result. ``'big'`` for
		  big-endian, ``'little'`` for little-endian.

		If you want byte- and word-ordering to differ, you're on your own.

		Using :ref:`string formatting` lets us use Python's C innards.
		"""

		# one (1) hex digit per four (4) bits
		width = val.bit_length()

		# unhexlify wants an even multiple of eight (8) bits, but we don't
		# want more digits than we need (hence the ternary-ish 'or')
		width += 8 - ((width % 8) or 8)

		# format width specifier: four (4) bits per hex digit
		fmt = '%%0%dx' % (width // 4)

		# prepend zero (0) to the width, to zero-pad the output
		s = unhexlify(fmt % val)

		if endianness == 'little':
			# see http://stackoverflow.com/a/931095/309233
			s = s[::-1]

		return s
			
	def rcvPacket(self):
		while True: 
			#print 'rcvPacket'
			if self.ser.read() == self.HEADER:        
				break
		hexlen = str(self.ser.read(4));
		hexdata = self.ser.read(int(hexlen, 16));
		return self.HEADER + hexlen + hexdata + self.TERM
	def SendData(self):
		# Send Head cmd
		mtAliveNotifyData = ""  
		R = []
		T = []
		tick = 0
		# while True: 
                
                start = time.time()
		while(self.Active):#Should use active

			sendCmd = "\x40\x30\x30\x30\x43\x0C\x00\xFF\x02\x07\x19\x80\x02" + self.long_to_bytes(current_milli_time(), "big") + self.TERM 
			#print "\nSendData sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in sendCmd)      
			self.ser.write(sendCmd)
			mtAliveNotifyData = self.rcvPacket()
			mtAliveNotifyData = ":".join("{:02x}".format(ord(c))for c in mtAliveNotifyData).split(":")
			#print "\nSendData replied (H<-D)"
			#print mtAliveNotifyData
			if len(mtAliveNotifyData) < 40:
				continue
			bearing = [int(mtAliveNotifyData[41]+mtAliveNotifyData[40],16)]
			tmp = map(lambda x : int(x,16),mtAliveNotifyData[44:-1])
			#tmp = list(CFAR(tmp,self.windowsize))
                        if self.Show_Log:
			        print tmp
                                print bearing
			Range,Dat = self.find_obj(tmp)
			R.append(Range)
			T.append(Dat)
			#print R
			if len(R) == self.dataSet:
				if not self.checkScantype:
                                        
					if tick==0:
						tick=1
					else:
						tick=0
					        R = R[::-1]
						T = T[::-1]
			# 	print msg
			#print len(tmp)
			#msg.dim = 1
				self.laser(R,T)
                                print time.time() - start
                                start = time.time()
                                print self.dataSet
				R = []
				T = []

	def HeadCommand(self):
		AliveNotifyData = ""
		headCmd = ("\x40\x30\x30\x34\x43\x4C\x00\xFF\x02\x47"
				   "\x13\x80\x02\x1D"+self.Scantype[1]+self.Scantype[0]+"\x02\x99\x99\x99"
				   "\x02\x66\x66\x66\x05\xA3\x70\x3D\x06\x70"
				   "\x3D\x0A\x09\x28\x00"+chr(self.Range[1])+chr(self.Range[0])+chr(self.Llim[1])+chr(self.Llim[0])+chr(self.Rlim[1])
				   +chr(self.Rlim[0])+chr(self.Adspan)+chr(self.Adlow)+chr(self.Gain)+chr(self.Gain)+"\x5A\x00\x7D\x00\x19"
				   +chr(self.Speed)+chr(self.Adinterval[1])+chr(self.Adinterval[0])+chr(self.nBin[1])+chr(self.nBin[0])+"\xE8\x03\x97\x03\x40"
				   "\x06\x01\x00\x00\x00\x50\x51\x09\x08\x54"
				   "\x54\x00\x00\x5A\x00\x7D\x00\x00\x00\x00"
				   "\x00\x0A")      
		while self.CheckAlive():  
			#print "\nHead sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in headCmd)
			#print 
			self.ser.write(headCmd)    
			AliveNotifyData = self.rcvPacket()
			#print "len ="
			#print len(AliveNotifyData)
			#print "\nHead replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in AliveNotifyData) + "\n"
			return
	#         if len(mtAliveNotifyData) == 22 and mtAliveNotifyData[10] == 1:
	def Reboot(self):
		# Send mtSendVersion cmd
		rebootCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x10\x80\x02\x0A"
		while self.CheckAlive():
			#print "\nReboot sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in rebootCmd) + "\n"
			#print 
			self.ser.write(rebootCmd)
			reboot_replied = self.rcvPacket()
			#print len(reboot_replied)
			#print "\nReboot replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in reboot_replied) + "\n"
			return
	def BBUser(self):
		# Send mtSendVersion cmd
		BBUserData = ""
		BBUserCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x18\x80\x02\x0A"
		while self.CheckAlive():
			#print "BBUser sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in BBUserCmd) + "\n"        
			#print 
			self.ser.write(BBUserCmd) 
			while True:    
				BBUserData = self.rcvPacket()
				#print "BBUser data (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in BBUserData)
				#print len(BBUserData)
				if BBUserData[self.MSG_index] == self.mtBBUserData:
					return
	def Version(self):
		# Send mtSendVersion cmd
		mtVersionData = ""
		versionCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x17\x80\x02\x0A"
		while self.CheckAlive():
			#print "version sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in versionCmd) + "\n"        
			#print 
			self.ser.write(versionCmd) 
			while True:    
				mtVersionData = self.rcvPacket()
				#print "version data (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in mtVersionData)
				#print len(mtVersionData)
				if len(mtVersionData) == 25:  # mtVersionData[MSG_index] == mtVersionData:
					return
	def CheckAlive(self):
		bAlive = False  # mtAlive msg rx
		while bAlive == False:  # Check sonar and connection
		
			serial_line = self.rcvPacket()
			#print "Alive replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in serial_line)
			#print len(mtVersionData)  #         if serial_line[10] = 0x4:      
			if serial_line[self.MSG_index] == self.mtAlive:
				# print time.strftime("hh:mm:ss", serial_line[13:17])
				return True
	def Callback(self,msg):
		"================================================="
		self.windowsize = int(round(self.CFARthres*msg.Range/msg.nBin))
		if self.windowsize == 0:
			self.windowsize = 1

		self.Active = msg.Active
		self.Speed = int(pow(2,msg.Speed+2))
		self.Gain = int(210 * msg.Gain /100)
		self.nBin = gen_bin(msg.nBin)
		self.Adlow = int(round(255 * msg.Adlow/80.0))
		self.Adspan = int(round(255 * msg.Adspan/80.0))
		self.Range = gen_range(msg.Range)
		self.Scantype = gen_scan(msg.Scantype)
		self.Llim = gen_lim(msg.Llim)
		self.Rlim = gen_lim(msg.Rlim)

                if msg.Scantype == 1:
                        self.dataSet = 6400 / self.Speed
                        print self.Speed
                        print self.dataSet
                else:
		        self.dataSet = ( msg.Rlim - msg.Llim ) / self.Speed
		self.Adinterval = gen_adinterval(msg.nBin,msg.Range)

		self.checkScantype = msg.Scantype
		self.checkRange = msg.Range
		self.checknBin = msg.nBin
			
		degree = (msg.Llim/16.0*0.9)
		if degree <= 180 :
			self.laser_angle_min = math.radians(180-(msg.Llim/16.0*0.9))
			self.laser_angle_max = math.radians(180-(msg.Rlim/16.0*0.9))
		elif degree > 180:
			self.laser_angle_min = math.radians((msg.Llim/16.0*0.9)-180)
			self.laser_angle_max = math.radians((msg.Rlim/16.0*0.9)-180)

		self.laser_increment = -math.radians((self.Speed/16.0)*0.9)
		self.time_increment = 0.2
		self.ranges = [i*float(msg.Range)/msg.nBin for i in range(msg.nBin)]

		rospy.loginfo("---- Sonar HeadCommand Start ----")
		sonarDriver.HeadCommand()      # use default head setting
		rospy.loginfo("---- Sonar HeadCommand Finish ----")
		rospy.loginfo("---- Sonar SendData Start ----")
		sonarDriver.SendData()
		rospy.loginfo("---- Sonar SendData Finish ----")
		rospy.loginfo("---- Sonar Wait For Command ----")
		
		#self.driverStatus()
		
	def driverStatus(self):
		print	self.Active
		print	self.Speed
		print	self.Gain
		print	self.nBin
		print	self.Adlow
		print	self.Adspan
		print	self.Range
		print	self.Scantype
		print	self.Llim
		print	self.Rlim,
		print	self.Adinterval

	def find_obj(self,d):
		#x = int(round(len(d)/self.checkRange*self.RangeThres))
		#print x
                #print len(d)
                #int(round(len(d)/self.checkRange*self.RangeThres)),len(d)):
		for i in xrange(self.BinStart,len(d)):
                        #print i
			if(d[i] > self.IntenThres):
				return self.ranges[i],d[i]
		return self.checkRange,0

	def laser(self,R,T):

		scan = LaserScan()
		timestamp = rospy.get_rostime()
		scan.header.stamp = timestamp

		scan.header.frame_id = "sonar_link"
		scan.angle_min = math.pi/2#self.laser_angle_min
		scan.angle_max = -math.pi/2#self.laser_angle_max
	
		scan.angle_increment = self.laser_increment
		scan.time_increment = 0#self.time_increment

		scan.scan_time = 8

		scan.range_min = 0
		scan.range_max = self.checkRange

		scan.ranges = R
		scan.intensities = T

		self.publisher.publish(scan)
	        rospy.loginfo("---- Sonar Publish ScanData ----")

		#print scan
		#print len(scan.ranges)
		#print len(scan.intensities)
		return
		
		

def gen_scan(dat):
	if dat == 1:
		return ['\x23','\x83']
	elif dat == 0:
		return ['\x23','\x81']

def gen_lim(dat):
	dat = "{:04x}".format(dat)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]

def gen_bin(dat):
	if dat > 1500:
		dat = 1500
	elif dat < 0:
		dat = 0
	
	dat = "{:04x}".format(dat + 1)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]

def gen_range(dat):
	if dat > 20:
		dat = 20
	elif dat < 20:
		dat = 2

	dat = '{:04x}'.format(dat*10)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]

def gen_adinterval(num_bin,num_range):
	adin = int(round(num_range * 2) / (1460.0 * ( num_bin + 1 ) * 640e-9))
	dat = "{:04x}".format(adin)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]	

# CFAR author -> 'Teerasit Kasetkasem'
def CFAR(line, r):
	kernel = np.zeros((3*r),'float32')
	kernel[0:r] = 1./(2.*r)
	kernel[2*r:3*r] = 1./(2.*r)


	output = []
	ll =np.array(line)
	ll = ll[::-1]
	patch_size = r+ r/2

	end_point = len(line)+patch_size
	line2 = np.zeros(len(line)+3*r-1,'float32')
	line2[:patch_size] = ll[len(line)-patch_size-1:-1]
	line2[end_point:end_point+patch_size] = ll[1:patch_size+1]
	line2[patch_size:end_point] = line

	out = np.convolve(line2, kernel)
	output = out[3*r-1:3*r+len(line)-1]
	#print list(out2)
	#print list(line)
	
	#out = out[0:len(line)]
	return output

if __name__ == '__main__':
	ser = serial.Serial(rospy.get_param("sonarDriver/serial_port"), 115200)
	rospy.init_node('test')
	publisher = rospy.Publisher('/scan',LaserScan,queue_size = 1)

	sonarDriver = sonarDriver(ser,publisher)
	
	# msg = [1,160,50,148,9,12,2,0,3200,6400]    

	# sonarDriver.Active = msg[0]
	# sonarDriver.Step = int((msg[9]-msg[8])/msg[1])
	# sonarDriver.Gain = int(210 * msg[2] /100)
	# sonarDriver.nBin = gen_bin(msg[3])
	# sonarDriver.Adlow = int(round(255 * msg[4]/80.0))
	# sonarDriver.Adspan = int(round(255 * msg[5]/80.0))
	# sonarDriver.Range = gen_range(msg[6])
	# sonarDriver.Scantype = gen_scan(msg[7])
	# sonarDriver.Llim = gen_lim(msg[8])
	# sonarDriver.Rlim = gen_lim(msg[9])
	# sonarDriver.Adinterval = gen_adinterval(msg[3],msg[6])
	rospy.loginfo("---- Sonar Reboot Start ----")
	sonarDriver.Reboot()
	rospy.loginfo("---- Sonar Reboot Finish ----")
	rospy.loginfo("---- Sonar Version Start ----")
	sonarDriver.Version()
	rospy.loginfo("---- Sonar Version Finish ----")
	rospy.loginfo("---- Sonar BBUser Start ----")
	sonarDriver.BBUser()
	rospy.loginfo("---- Sonar BBUser Finish ----")
	rospy.loginfo("---- Sonar Wait For Command ----")
	#while not rospy.is_shutdown():
	rospy.spin()
	#ser.close()  # Only executes once the loop exits
