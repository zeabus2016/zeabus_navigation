import rospy
from sensor_msgs.msg import LaserScan

import math
from std_msgs.msg import Int64MultiArray,Float32MultiArray
#from sonar_fake import get_data_from_csv


rospy.init_node("test")

rospy.loginfo("in scan pub")
scanPub = rospy.Publisher('/scan', LaserScan,queue_size = 10)
print "trying stamps first header then scan_now"
# had issues with getting scan_now and the header to work



MAX_RANGE = 5


def find(d):
    
    T = 80
    for i in xrange(20,len(d)):
        if(d[i] > T):
            return i/148.0*5
    
    return MAX_RANGE        

#D = get_data_from_csv()

frame_id = "sonar_link"
MIN = math.pi/2
MAX = -math.pi/2
INC = -math.pi/160.0
#print D


def scan_callback(data):
    data = data.data
    
    print data,"HERE"
    
    scan = LaserScan()
    scan_now = rospy.get_rostime()
    scan.header.stamp = scan_now

    #print scan.header.stamp
    scan.header.frame_id = frame_id
    scan.angle_min = MIN#-math.pi/2
    scan.angle_max = MAX#math.pi/2
    scan.angle_increment = INC#math.pi/nub
    scan_rate = 1
    scan.scan_time = scan_rate
    scan.range_min = 0.20
    scan.range_max = 10
    # scan.ranges is a list ( or array)
    scan.ranges = [ 1 ]
    #print "check frame.scan"

    size = len(data)
    
    scan.ranges = [0]*(size+1)
    scan.intensities = [0] * (size+1)
    count = 0;
    #data = D[di]
    #di+=1
    for i in xrange(size):
        scan.ranges[i] = data[i]
        
        if(data[i] != MAX_RANGE):
            scan.intensities[i] = 100
    
    """
    for i in range(nub+1):
        print 20*scan.angle_increment
        scan.ranges[i] = 2.5
        scan.intensities[i] = 100;
        count+=1
    """
    
    print scan
    scanPub.publish(scan) 

# i have a problem with the usb buffers on my arm/ubuntu controller on the robot which seems to drop the range data
# the empty range data was causing gmapping gslam to crash so checking to make sure I have range data

i=-1


rospy.Subscriber("/sonar/sonar",Float32MultiArray,scan_callback)
    
#pub_data = rospy.Publisher('sonar/sonar',Int64MultiArray,queue_size = 10)
#msg = Int64MultiArray()

"""
while not rospy.is_shutdown():
    for i in xrange()
        msg.data = range(10)
    pub_data.publish(msg)
    rospy.sleep(0.1)
"""
rospy.spin()
    


"""

 

"""
