#!/usr/bin/env python
'''
Created on Jan 22, 2015

@author: anol
'''

import rospy
import serial, time
import CFAR
from binascii import unhexlify

from std_msgs.msg import Int64MultiArray
from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray

current_milli_time = lambda: int(round(time.time()))

Active = None
nBin = None
Gain = None
Adlow = None
Adspan = None
Range = None
Scantype = None
Step = None
Adinterval = None
	
init_cmd = False

HEADER = '\x40'
TERM = '\x0A'

# Byte index
HEADER_index = 0
HHHH_index = 2
BB_index = 5
SID_index = 7
DID_index = 8
COUNT_index = 9
MSG_index = 10

# Mostly using command
mtVersionData = '\x01'  # 1
mtHeadData = '\x02'  # 2
mtAlive = '\x04'  # 4
mtBBUserData = '\x06'  # 6
mtReboot = '\x10'  # 16
mtHeadCmd = '\x13'  # 19
mtSendVersion = '\x17'  # 23
mtSendBBUser = '\x18'  # 24
mtSendData = '\x19'  # 25
mtStopAlives = '\x42'  # 66

def find_ob(d):
	
	T = 80
	for i in xrange(20,len(d)):
		if(d[i] > T):
			return i/148.0*5
	
	return 5


def long_to_bytes (val, endianness='big'):
	"""
	Use :ref:`string formatting` and :func:`~binascii.unhexlify` to
	convert ``val``, a :func:`long`, to a byte :func:`str`.

	:param long val: The value to pack

	:param str endianness: The endianness of the result. ``'big'`` for
	  big-endian, ``'little'`` for little-endian.

	If you want byte- and word-ordering to differ, you're on your own.

	Using :ref:`string formatting` lets us use Python's C innards.
	"""

	# one (1) hex digit per four (4) bits
	width = val.bit_length()

	# unhexlify wants an even multiple of eight (8) bits, but we don't
	# want more digits than we need (hence the ternary-ish 'or')
	width += 8 - ((width % 8) or 8)

	# format width specifier: four (4) bits per hex digit
	fmt = '%%0%dx' % (width // 4)

	# prepend zero (0) to the width, to zero-pad the output
	s = unhexlify(fmt % val)

	if endianness == 'little':
		# see http://stackoverflow.com/a/931095/309233
		s = s[::-1]

	return s


def rcvPacket(ser):
	while True: 
		if ser.read() == HEADER:        
			break
	hexlen = str(ser.read(4));
	hexdata = ser.read(int(hexlen, 16));
	return HEADER + hexlen + hexdata + TERM

def SendData(ser,pub_data):
	# Send Head cmd
	mtAliveNotifyData = ""  
	msg = Float32MultiArray()
	tick = 0
	tmp = []
	
	sonar_output = []
	# while True: 
	while(Active):#Should use active
		sendCmd = "\x40\x30\x30\x30\x43\x0C\x00\xFF\x02\x07\x19\x80\x02" + long_to_bytes(current_milli_time(), "big") + TERM 
		#print "\nSendData sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in sendCmd)      
		ser.write(sendCmd)
		mtAliveNotifyData = rcvPacket(ser)
		mtAliveNotifyData = ":".join("{:02x}".format(ord(c))for c in mtAliveNotifyData).split(":")
		#print "\nSendData replied (H<-D)"
		try:
			tmp = [int(mtAliveNotifyData[41]+mtAliveNotifyData[40],16)]
			tmp+=(map(lambda x : int(x,16),mtAliveNotifyData[44:-1]))
			#print tmp
			
			
			
			tmp = find_ob(tmp)
			print tmp   
			msg.data.append(tmp)
			#print  len(msg.data), len(msg.data)==161
			
			
			if len(msg.data)==161:
				if tick==0:
					tick=1
				else:
					tick=0
					msg.data = msg.data[::-1]
				#msg = CFAR(161,msg)
				#print "HERE"*50
				
				
				print msg
				#print "1111"*50
				pub_data.publish(msg)
				#print "2222"*50
				#rospy.sleep(0.1)
				#print "3333"*50
				'''print '##############################PREVIEW-DATA##############################'
				print msg.data[0]
				print msg.data[-1]
				rospy.sleep(0.1)'''
				msg.data = [tmp]
				#print "4444"*50
				
		except:
			pass
	#print len(mtAliveNotifyData)

def HeadCommand(ser):
	AliveNotifyData = ""
	headCmd = ("\x40\x30\x30\x34\x43\x4C\x00\xFF\x02\x47"
			   "\x13\x80\x02\x1D"+Scantype[1]+Scantype[0]+"\x02\x99\x99\x99"
			   "\x02\x66\x66\x66\x05\xA3\x70\x3D\x06\x70"
			   "\x3D\x0A\x09\x28\x00"+chr(Range[1])+chr(Range[0])+chr(Llim[1])+chr(Llim[0])+chr(Rlim[1])
			   +chr(Rlim[0])+chr(Adspan)+chr(Adlow)+chr(Gain)+chr(Gain)+"\x5A\x00\x7D\x00\x19"
			   +chr(Step)+chr(Adinterval[1])+chr(Adinterval[0])+chr(nBin[1])+chr(nBin[0])+"\xE8\x03\x97\x03\x40"
			   "\x06\x01\x00\x00\x00\x50\x51\x09\x08\x54"
			   "\x54\x00\x00\x5A\x00\x7D\x00\x00\x00\x00"
			   "\x00\x0A")      
	while CheckAlive(ser):  
		print "\nHead sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in headCmd)
		print ser.write(headCmd)    
		AliveNotifyData = rcvPacket(ser)
		print "len ="
		print len(AliveNotifyData)
		print "\nHead replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in AliveNotifyData) + "\n"
		return
#         if len(mtAliveNotifyData) == 22 and mtAliveNotifyData[10] == 1:
	 

def Reboot(ser):
	# Send mtSendVersion cmd
	rebootCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x10\x80\x02\x0A"
	while CheckAlive(ser):
		print "\nReboot sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in rebootCmd) + "\n"
		print ser.write(rebootCmd)
		reboot_replied = rcvPacket(ser)
		print len(reboot_replied)
		print "\nReboot replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in reboot_replied) + "\n"
		return

def StopAlives(ser):
	# Send mtSendVersion cmd
	rebootCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x42\x80\x02\x0A"
	while CheckAlive(ser):
		print "\nStopAlives sending (H->D)" + ":".join("{:02x}".format(ord(c)) for c in rebootCmd) + "\n"
		print ser.write(rebootCmd)
#         reboot_replied = rcvPacket(ser)
#         print len(reboot_replied)
#         print "\nStopAlives replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in reboot_replied) + "\n"
		return

def BBUser(ser):
	# Send mtSendVersion cmd
	BBUserData = ""
	BBUserCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x18\x80\x02\x0A"
	while CheckAlive(ser):
		print "BBUser sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in BBUserCmd) + "\n"        
		print ser.write(BBUserCmd) 
		while True:    
			BBUserData = rcvPacket(ser)
			print "BBUser data (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in BBUserData)
			print len(BBUserData)
			if BBUserData[MSG_index] == mtBBUserData:
				return


def Version(ser):
	# Send mtSendVersion cmd
	mtVersionData = ""
	versionCmd = "\x40\x30\x30\x30\x38\x08\x00\xFF\x02\x03\x17\x80\x02\x0A"
	while CheckAlive(ser):
		print "version sending (H->D)\n" + ":".join("{:02x}".format(ord(c)) for c in versionCmd) + "\n"        
		print ser.write(versionCmd) 
		while True:    
			mtVersionData = rcvPacket(ser)
			print "version data (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in mtVersionData)
			print len(mtVersionData)
			if len(mtVersionData) == 25:  # mtVersionData[MSG_index] == mtVersionData:
				return

def CheckAlive(ser):
	bAlive = False  # mtAlive msg rx
	while bAlive == False:  # Check sonar and connection
	
		serial_line = rcvPacket(ser)
		print "Alive replied (H<-D)\n" + ":".join("{:02x}".format(ord(c)) for c in serial_line)
		print len(mtVersionData)  #         if serial_line[10] = 0x4:      
		if serial_line[MSG_index] == mtAlive:
			# print time.strftime("hh:mm:ss", serial_line[13:17])
			return True

def gen_scan(dat):
	if dat == 1:
		return ['\x23','\x83']
	elif dat == 0:
		return ['\x23','\x81']
def gen_lim(dat):
	dat = "{:04x}".format(dat)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]
def gen_bin(dat):
	if dat > 1500:
		dat = 1500
	elif dat < 0:
		dat = 0
	
	dat = "{:04x}".format(dat + 1)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]
def gen_range(dat):
	if dat > 20:
		dat = 20
	elif dat < 20:
		dat = 2

	dat = '{:04x}'.format(dat*10)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]
def gen_adinterval(num_bin,num_range):
	adin = int(round(num_range * 2) / (1460.0 * ( num_bin + 1 ) * 640e-9))
	dat = "{:04x}".format(adin)
	return [int("0x" + dat[0:2] , 0),
		int("0x" + dat[2:4] , 0)]



if __name__ == '__main__':
	global Active,Step,Gain,nBin,Adlow,Adspan,Range,Scantype,Llim,Rlim,Adinterval
	ser = serial.Serial('/dev/ttyUSB0', 115200)
	rospy.init_node('sonar_serial')
	pub_data = rospy.Publisher('sonar/sonar',Float32MultiArray,queue_size = 10)

	
	
	msg = [1,160,50,148,9,12,5,0,3200,6400]    
	Active = msg[0]
	Step = int((msg[9]-msg[8])/msg[1])
	Gain = int(210 * msg[2] /100)
	nBin = gen_bin(msg[3])
	Adlow = int(round(255 * msg[4]/80.0))
	Adspan = int(round(255 * msg[5]/80.0))
	Range = gen_range(msg[6])
	Scantype = gen_scan(msg[7])
	Llim = gen_lim(msg[8])
	Rlim = gen_lim(msg[9])
	Adinterval = gen_adinterval(msg[3],msg[6])
	
	Reboot(ser)
	Version(ser)
	BBUser(ser)
	
	print Active,Step,Gain,nBin,Adlow,Adspan,Range,Scantype,Llim,Rlim,Adinterval
	if Active:
		while not rospy.is_shutdown():
			HeadCommand(ser)      # use default head setting
			SendData(ser,pub_data)
			rospy.sleep(1)
	rospy.spin()
	ser.close()  # Only executes once the loop exits
