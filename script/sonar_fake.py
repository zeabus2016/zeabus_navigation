import csv

def find(d):
    
    T = 80
    for i in xrange(20,len(d)):
        if(d[i] > T):
            return i/148.0*5
    
    return 5        
    
def get_data_from_csv():
    n = 100
    D = []
    with open('test.csv') as f:
        r = csv.DictReader(f) # read rows into a dictionary format

        for i in list(r)[3:]:
            b = int(i['Bearing'])
            
            data = find(map(int,i[None]))

            #print b,data
            if b<= 2800:
                if D:
                    D[-1]=D[-1][::-1]
                    #D.append(b)
                D.append([])
            
            #print b,data
            D[-1]+=[data]
            if b >= 3600:
                D.append([])
            #n-=1
            #if not n:
            #    break    
         
    return D

#D = get_data_from_csv()
#print D[2]
    
